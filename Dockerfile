FROM python:3.6.5-slim

WORKDIR /usr/src/app

#add and install requirements
COPY ./requirements.txt /usr/src/app/requirements.txt

RUN apt-get update -y && \
    apt-get install -y gnupg2 && \
    apt-get install -y apt-transport-https ca-certificates && \
    python3 -m pip install --upgrade pip && \
    python3 -m pip install -r requirements.txt --no-cache-dir && \
    echo 'deb http://apt-archive.postgresql.org/pub/repos/apt/ stretch-pgdg main' >  /etc/apt/sources.list.d/pgdg.list  && \
	apt-get clean && apt-get install -y wget  && \
	wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | apt-key add -  && \
	apt-get update  && \
	yes Y | apt-get install postgresql-client